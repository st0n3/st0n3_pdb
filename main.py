import sys
import marshal
from cfg.cfg import Cfg
from util.graph import render_graph
# sys.setrecursionlimit(81920)

filename = sys.argv[1]
f = open(filename, 'rb')
f.seek(8)
co = marshal.load(f)
cfg = Cfg(co)
render_graph(cfg.graph, 'before')
print 'len', cfg.graph.number_of_nodes()
cfg.optimize(cfg.entry_point)
print 'len', cfg.graph.number_of_nodes()
render_graph(cfg.graph, 'after')
