import types
from dis import opname, HAVE_ARGUMENT, hasconst, hasname, hasjrel, haslocal, hascompare, cmp_op


def disassemble_string(code, lasti=-1, varnames=None, names=None,
                       constants=None):
    n = len(code)
    i = 0
    while i < n:
        c = code[i]
        op = ord(c)
        print ' ' * 10,
        print repr(i + lasti).rjust(4),
        print opname[op].ljust(15),
        i = i + 1
        if op >= HAVE_ARGUMENT:
            oparg = ord(code[i]) + ord(code[i + 1]) * 256
            i = i + 2
            print repr(oparg).rjust(5),
            if op in hasconst:
                if constants:
                    print '(' + repr(constants[oparg]) + ')',
                else:
                    print '(%d)' % oparg,
            elif op in hasname:
                if names is not None:
                    print '(' + names[oparg] + ')',
                else:
                    print '(%d)' % oparg,
            elif op in hasjrel:
                print '(to ' + repr(i + lasti + oparg) + ')',
            elif op in haslocal:
                if varnames:
                    print '(' + varnames[oparg] + ')',
                else:
                    print '(%d)' % oparg,
            elif op in hascompare:
                print '(' + cmp_op[oparg] + ')',
        print
