import opcode
import pystack
from log import logger

JUMP = [opcode.opmap['JUMP_ABSOLUTE'], opcode.opmap['JUMP_FORWARD']]
POP_JUMP = [opcode.opmap['POP_JUMP_IF_TRUE'], opcode.opmap['POP_JUMP_IF_FALSE']]
DIVIDER = -1


def jump_until_target(code, i):
    op = ord(code[i])
    if op >= opcode.HAVE_ARGUMENT:
        oparg = ord(code[i + 1]) + ord(code[i + 2]) * 256
        if op == opcode.opmap['JUMP_ABSOLUTE']:
            logger.debug("{} {} {}".format(i, opcode.opname[op], oparg))
            return jump_until_target(code, oparg)
        elif op == opcode.opmap['JUMP_FORWARD']:
            logger.debug("{} {} {}".format(i, opcode.opname[op], oparg))
            return jump_until_target(code, i + oparg + 3)
    # i dont know why only i+1 can trigger bp
    return i


def check_divider_jump(op, const, divider):
    return (const == divider) == (POP_JUMP.index(op) == 0)


def bypass_divider_continuous(code, lasti, varnames, consts):
    global DIVIDER
    lasti = jump_until_target(code, lasti)
    i = lasti
    op = ord(code[i])
    if op >= opcode.HAVE_ARGUMENT and op == opcode.opmap['LOAD_CONST']:
        oparg = ord(code[i + 1]) + ord(code[i + 2]) * 256
        const = consts[oparg]
        logger.debug("{} {} {} ({})".format(i, opcode.opname[op], oparg, const))
        i += 3

        i = jump_until_target(code, i)
        op = ord(code[i])
        if op >= opcode.HAVE_ARGUMENT and op == opcode.opmap['STORE_FAST']:
            oparg = ord(code[i + 1]) + ord(code[i + 2]) * 256
            logger.debug("{} {} {}".format(i, opcode.opname[op], oparg))
            i += 3
            if varnames[oparg] == 'DIVIDER':
                DIVIDER = const
                return bypass_divider_continuous(code, i, varnames, consts)
        elif op >= opcode.HAVE_ARGUMENT and op == opcode.opmap['LOAD_FAST']:
            oparg = ord(code[i + 1]) + ord(code[i + 2]) * 256
            logger.debug("{} {} {}".format(i, opcode.opname[op], oparg))
            if varnames[oparg] == 'DIVIDER':
                i += 3

                i = jump_until_target(code, i)
                op = ord(code[i])
                if op >= opcode.HAVE_ARGUMENT and op == opcode.opmap['COMPARE_OP']:
                    oparg = ord(code[i + 1]) + ord(code[i + 2]) * 256
                    logger.debug("{} {} {}".format(i, opcode.opname[op], oparg))
                    if oparg == 2:
                        i += 3

                        i = jump_until_target(code, i)
                        op = ord(code[i])
                        if op >= opcode.HAVE_ARGUMENT and op in POP_JUMP:
                            oparg = ord(code[i + 1]) + ord(code[i + 2]) * 256
                            logger.debug("{} {} {}".format(i, opcode.opname[op], oparg))
                            i += 3
                            if check_divider_jump(op, const, DIVIDER):
                                return bypass_divider_continuous(code, oparg, varnames, consts)
                                # return oparg + 1
                            else:
                                return bypass_divider_continuous(code, i, varnames, consts)
                                # return i + 1
    return lasti


def bypass_store_divider(code, lasti, consts):
    global DIVIDER
    lasti = jump_until_target(code, lasti)
    i = lasti
    op = ord(code[i])
    if op >= opcode.HAVE_ARGUMENT and op == opcode.opmap['LOAD_CONST']:
        oparg = ord(code[i + 1]) + ord(code[i + 2]) * 256
        const = consts[oparg]
        logger.debug("{} {} {} ({})".format(i, opcode.opname[op], oparg, const))
        i += 3

        i = jump_until_target(code, i)
        op = ord(code[i])
        if op >= opcode.HAVE_ARGUMENT and op == opcode.opmap['STORE_FAST']:
            pass

    return


def bypass(frame):
    global DIVIDER
    co = frame.f_code
    code = co.co_code
    lasti = frame.f_lasti
    varnames = co.co_varnames
    consts = co.co_consts
    DIVIDER = frame.f_locals['DIVIDER'] if 'DIVIDER' in frame.f_locals else None

    # jump
    i = jump_until_target(code, lasti)
    i = bypass_divider_continuous(code, i, varnames, consts)
    return i


def bypass_for_iter(code, i):
    op = ord(code[i])
    oparg = ord(code[i + 1]) + ord(code[i + 2]) * 256
    target = i + 3 + oparg
    logger.info("{} {} {} ({})".format(i, opcode.opname[op], oparg, target))
    return target
