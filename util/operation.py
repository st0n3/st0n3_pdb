import types
import opcode
import st0n3_dis


class Operation:
    def __init__(self, code, offset, code_object):
        self.offset = offset
        self.code_object = code_object
        self.name = str(self.offset)
        self.code = code
        self.op = ord(self.code[0])
        self.size = 1 if self.op < opcode.HAVE_ARGUMENT else 3
        self.arg = ord(self.code[1]) + ord(self.code[2]) * 256

    def is_jump_directly(self):
        return self.op in [opcode.opmap['JUMP_ABSOLUTE'], opcode.opmap['JUMP_FORWARD']]

    def is_control_flow(self):
        return self.op in opcode.hasjrel or self.op in opcode.hasjabs

    def get_target(self):
        if self.is_control_flow():
            if self.op == opcode.opmap['JUMP_ABSOLUTE']:
                return self.arg
            if self.op == opcode.opmap['JUMP_FORWARD']:
                return self.offset + self.size + self.arg
            return self.offset + self.size
        else:
            return self.offset + self.size

    @property
    def label(self):
        # print '[debug]', self.code
        # label = "off_{:<6s}{:<25s}{:<10s}".format(
        #     str(self.offset), opcode.opname[self.op], str(self.arg)
        # )
        # if self.op == opcode.opmap['JUMP_FORWARD']:
        #     label += '(to {})'.format(self.get_target())
        # return label
        return self.disassemble_string()

    def is_return(self):
        return self.op == opcode.opmap['RETURN_VALUE']

    def has_branch(self):
        return self.op in [opcode.opmap['POP_JUMP_IF_TRUE'], opcode.opmap['POP_JUMP_IF_FALSE']]

    def next_offset(self):
        if self.is_control_flow():
            if self.has_branch():
                return [self.offset + self.size, self.arg]
            elif self.op == opcode.opmap['JUMP_ABSOLUTE']:
                return [self.arg]
            elif self.op == opcode.opmap['JUMP_FORWARD']:
                return [self.offset + self.size + self.arg]
        return [self.offset + self.size]

    def disassemble_string(self):
        asm = "off_{:<6d}{:<15s} ".format(self.offset, opcode.opname[self.op])
        if self.size == 3:
            asm += str(self.arg)
        if self.op in opcode.hasconst:
            const = self.code_object.co_consts[self.arg]
            if isinstance(const, types.CodeType):
                asm += "(code object {})".format(const.co_name)
            else:
                asm += "({})".format(repr(const))
        elif self.op in opcode.hasname:
            asm += '(' + self.code_object.co_names[self.arg] + ')'
        elif self.op in opcode.hasjrel:
            asm += '(to {})'.format(repr(self.offset + self.arg))
        elif self.op in opcode.haslocal:
            asm += '(' + self.code_object.co_varnames[self.arg] + ')'
        elif self.op in opcode.hascompare:
            asm += '({})'.format(opcode.cmp_op[self.arg])
        return asm


def nop_op(res, offset):
    code = "\x09\x00" + ("\x01" if res else "\x00")
    return Operation(code, offset, None)
