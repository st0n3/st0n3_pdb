import dis
import bdb
import opcode
import inspect
import pystack
import st0n3_dis
import deobfuscator
from pdb import Pdb

import lie


def run(statement, fake_globals, fake_locals, globals=None, locals=None):
    new_co = lie.lnotab_all(statement)
    St0n3Pdb(new_co.co_filename, fake_globals, fake_locals).run(new_co, globals, locals)


class St0n3Pdb(Pdb):
    def __init__(self, filename, fake_globals, fake_locals, completekey='tab', stdin=None, stdout=None, skip=None):
        self.filename = filename
        self.fake_globals = fake_globals
        self.fake_locals = fake_locals
        Pdb.__init__(self, completekey, stdin, stdout, skip)
        self.prompt = self.prompt.replace('Pdb', 'st0n3-pdb')

    def do_step(self, arg):
        if self.curframe.f_code.co_filename == self.filename:
            lasti = self.curframe.f_lasti
            code = self.curframe.f_code.co_code
            varnames = self.curframe.f_code.co_varnames
            names = self.curframe.f_code.co_names
            constants = self.curframe.f_code.co_consts

            print '[lasti]', lasti
            self.do_stack(arg)
            op = ord(code[lasti])
            if op < opcode.HAVE_ARGUMENT:
                st0n3_dis.disassemble_string(code[lasti:lasti + 1], lasti, varnames, names, constants)
                return Pdb.do_step(self, arg)
            else:
                if op in deobfuscator.JUMP or op == opcode.opmap['LOAD_CONST']:
                    target = deobfuscator.bypass(self.curframe)
                    if target != lasti:
                        self.clear_all_breaks()
                        self.do_break(str(target + 1))
                        self._set_stopinfo(self.curframe, self.curframe, -1)
                        return 1
                elif op == opcode.opmap['FOR_ITER']:
                    print '[debug] ??????????'
                    st0n3_dis.disassemble_string(code[lasti:lasti + 3], lasti, varnames, names, constants)
                    target = deobfuscator.bypass_for_iter(code, lasti)
                    self.clear_all_breaks()
                    self.do_break(str(target + 1))
                    self._set_stopinfo(self.curframe, self.curframe, -1)
                    return 1
                st0n3_dis.disassemble_string(code[lasti:lasti + 3], lasti, varnames, names, constants)
                return Pdb.do_step(self, arg)
        else:
            return Pdb.do_return(self, arg)

    do_s = do_step

    def do_stack(self, arg):
        size = pystack.getStackSize(self.curframe)
        print '[debug][size]', size
        stack = []
        if '__return__' in self.curframe.f_locals:
            print '[debug] return but not get stack'
            return
        for i in range(size):
            stack.append(pystack.getStackItem(self.curframe, i))
        print '[stack]', stack

    def do_inspect_frame(self, arg):
        print '[debug] inspect frame'
        print inspect.getmembers(self.curframe)

    def do_list_locals(self, arg):
        print '[locals]'
        for k, v in self.curframe.f_locals.items():
            if k not in self.fake_locals:
                print k, '=', v
    do_ll = do_list_locals

    def do_list_globals(self, arg):
        print '[globals]'
        for k, v in self.curframe.f_globals.items():
            if k not in self.fake_globals:
                print k, '=', v
    do_lg = do_list_globals

    def do_break(self, arg, temporary=0):
        if not arg:
            if self.breaks:  # There's at least one
                print >>self.stdout, "Num Type         Disp Enb   Where"
                for bp in bdb.Breakpoint.bpbynumber:
                    if bp:
                        bp.bpprint(self.stdout)
            return
        # parse arguments; comma has lowest precedence
        # and cannot occur in filename
        filename = None
        lineno = None
        cond = None
        comma = arg.find(',')
        if comma > 0:
            # parse stuff after comma: "condition"
            cond = arg[comma+1:].lstrip()
            arg = arg[:comma].rstrip()
        # parse stuff before comma: [filename:]lineno | function
        colon = arg.rfind(':')
        funcname = None
        if colon >= 0:
            filename = arg[:colon].rstrip()
            f = self.lookupmodule(filename)
            if not f:
                print >>self.stdout, '*** ', repr(filename),
                print >>self.stdout, 'not found from sys.path'
                return
            else:
                filename = f
            arg = arg[colon+1:].lstrip()
            try:
                lineno = int(arg)
            except ValueError, msg:
                print >>self.stdout, '*** Bad lineno:', arg
                return
        else:
            # no colon; can be lineno or function
            try:
                lineno = int(arg)
            except ValueError:
                try:
                    func = eval(arg,
                                self.curframe.f_globals,
                                self.curframe_locals)
                except:
                    func = arg
                try:
                    if hasattr(func, 'im_func'):
                        func = func.im_func
                    code = func.func_code
                    #use co_name to identify the bkpt (function names
                    #could be aliased, but co_name is invariant)
                    funcname = code.co_name
                    lineno = code.co_firstlineno
                    filename = code.co_filename
                except:
                    # last thing to try
                    (ok, filename, ln) = self.lineinfo(arg)
                    if not ok:
                        print >>self.stdout, '*** The specified object',
                        print >>self.stdout, repr(arg),
                        print >>self.stdout, 'is not a function'
                        print >>self.stdout, 'or was not found along sys.path.'
                        return
                    funcname = ok # ok contains a function name
                    lineno = int(ln)
        if not filename:
            filename = self.defaultFile()
        # Check for reasonable breakpoint
        # line = self.checkline(filename, lineno)
        line = lineno
        if line:
            # now set the break point
            # err = self.set_break(filename, line, temporary, cond, funcname)

            filename = self.canonic(filename)
            import linecache  # Import as late as possible
            # line = linecache.getline(filename, lineno)
            # if not line:
            #     err = 'Line %s:%d does not exist' % (filename,
            #                                           lineno)
            if not filename in self.breaks:
                self.breaks[filename] = []
            list = self.breaks[filename]
            if not lineno in list:
                list.append(lineno)
            bp = bdb.Breakpoint(filename, lineno, temporary, cond, funcname)

            # if err: print >>self.stdout, '***', err
            # else:
            bp = self.get_breaks(filename, line)[-1]
            print >>self.stdout, "Breakpoint %d at %s:%d" % (bp.number,
                                                             bp.file,
                                                             bp.line)
    do_b = do_break

    def do_break_until(self, arg):
        self.clear_all_breaks()
        return self.do_break(arg)
