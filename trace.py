import marshal
import opcode
import sys

import lie
import st0n3_dis

FILENAME = ''
LAST = -1
DEOBFUSCATE = {}


def dispatch_all(frame):
    lasti = frame.f_lasti
    code = frame.f_code.co_code
    varnames = frame.f_code.co_varnames
    names = frame.f_code.co_names
    constants = frame.f_code.co_consts
    op = ord(code[lasti])
    if op < opcode.HAVE_ARGUMENT:
        st0n3_dis.disassemble_string(code[lasti:lasti + 1], lasti, varnames, names, constants)
    else:
        st0n3_dis.disassemble_string(code[lasti:lasti + 3], lasti, varnames, names, constants)

    return trace


def dispatch_call(frame, arg):
    pass


def dispatch_return(frame, arg):
    return trace


def dispatch_exception(frame, arg):
    return trace


def trace(frame, event, arg):
    # global LAST
    # if LAST == frame.f_lasti:
    #     LAST = frame.f_lasti
    #     return trace
    # LAST = frame.f_lasti
    # if FILENAME == frame.f_code.co_filename:
    #     if frame.f_code.co_name == '<module>':
    #         return
    #     print frame.f_code.co_name
    #     return dispatch_all(frame)
    return trace

    # if event == 'line':
    #     return dispatch_line(frame)
    # elif event == 'call':
    #     pass
    #     # return dispatch_call(frame, arg)
    # elif event == 'return':
    #     return dispatch_return(frame, arg)
    # elif event == 'exception':
    #     return dispatch_exception(frame, arg)
    # elif event == 'c_call':
    #     pass
    # elif event == 'c_exception':
    #     pass
    # elif event == 'c_return':
    #     pass
    # else:
    #     print 'bdb.Bdb.dispatch: unknown debugging event:', repr(event)
    # return trace


filename = sys.argv[1]
f = open(filename, 'rb')
f.seek(8)
co = marshal.load(f)
co = lie.lnotab_all(co)
FILENAME = co.co_filename
sys.settrace(trace)
exec co
