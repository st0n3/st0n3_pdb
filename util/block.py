import opcode
from util.operation import Operation


class Block:
    def __init__(self, init_op):
        assert isinstance(init_op, Operation)
        self.init_node = init_op
        self.block = [init_op]
        self.name = init_op.name

    def len(self):
        return len(self.block)

    def start_offset(self):
        first_op = self.block[0]
        return first_op.offset

    def next_offset(self):
        last_op = self.block[-1]
        return last_op.next_offset()

    def has_return(self):
        return self.block[-1].is_return()

    def combine(self, another_block):
        for op in another_block.block:
            self.block.append(op)

    def is_jump_directly(self):
        return self.len() == 1 and self.block[0].is_jump_directly()

    def is_divider_obfuscator(self, varnames):
        if self.len() == 3:
            op1, op2, op3 = self.block[0], self.block[1], self.block[2]
            if op1.op == opcode.opmap["LOAD_CONST"]:
                if op2.op == opcode.opmap["LOAD_FAST"]:
                    if op3.op == opcode.opmap["COMPARE_OP"]:
                        if varnames[op2.arg] == "DIVIDER":
                            if op3.arg == 2:
                                return True
        return False

    def should_be_optimized(self, varnames):
        return self.is_jump_directly() or self.is_divider_obfuscator(varnames)

    def has_divider(self, varnames):
        for op in self.block:
            if op.op in [opcode.opmap["STORE_FAST"], opcode.opmap["LOAD_FAST"]] and varnames[op.arg] == "DIVIDER":
                return True

    def update_divider(self, varnames, consts, divider):
        return False
        if self.has_divider(varnames):
            stack = []
            for op in self.block:
                if op.op == opcode.opmap["LOAD_CONST"]:
                    stack.append(consts[op.arg])
                if op.op == opcode.opmap["STORE_FAST"]:
                    divider = stack.pop()
        return divider

    def divider_equal(self, consts, divider):
        """
        used after self.is_divider_obfuscator
        :return:
        """
        print '[debug] divider=', divider
        load_const_op = self.block[0]
        return consts[load_const_op.arg] == divider
