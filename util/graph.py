import graphviz
import networkx

from util.block import Block


def render_graph(graph, filename):
    assert isinstance(graph, networkx.DiGraph)
    dot = graphviz.Digraph()
    for block in graph.nodes:
        assert isinstance(block, Block)
        label = ''
        for op in block.block:
            label += op.label + '\l'
        dot.node(name=block.name, label=label, shape='box', style='filled', color='black', fontcolor='white')

    for edge in graph.edges:
        src = edge[0].name
        dest = edge[1].name
        dot.edge(src, dest)

    # print 'render_graph dot'
    # dot.render(filename, format='dot')
    print 'render_graph'
    dot.render(filename, format='svg')
    # dot.render(filename, format='jpg', renderer='cairo')
