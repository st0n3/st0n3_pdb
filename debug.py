import sys
import marshal
import st0n3_pdb

filename = sys.argv[1]
f = open(filename, 'rb')
f.seek(8)
co = marshal.load(f)
st0n3_pdb.run(co, dict(globals().items()), dict(locals().items()))
