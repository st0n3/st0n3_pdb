import types

import networkx as nx

from util import operation
from util.block import Block
from util.operation import Operation


class Cfg:
    def __init__(self, code_object):
        assert isinstance(code_object, types.CodeType)
        self.code_object = code_object
        self.analysed_nodes = {}
        # avoid out of range
        self.code = code_object.co_code + '\x09\x09'
        self.consts = code_object.co_consts
        self.varnames = code_object.co_varnames
        self.graph = nx.DiGraph()
        self.entry_point = Block(Operation(self.code[0:3], 0, code_object))
        self.gen_graph_recursive(self.entry_point)
        # self.gen_graph_loop(self.entry_point)

    def gen(self):
        target = [0]
        i = 0
        last_node = None
        op = Operation(self.code[i:i + 3], i, self.code_object)
        while not op.is_return():
            if len(target) == 1:
                i = target[0]
            op = Operation(self.code[i:i + 3], i, self.code_object)
            block = Block(op)
            if op.is_control_flow():
                target = op.get_target()
            else:
                while not op.is_return():
                    i = op.get_target()
                    op = Operation(self.code[i:i + 3], i, self.code_object)
                    if op.is_control_flow():
                        break
                    block.block.append(op)

            self.graph.add_node(block)
            if last_node:
                self.graph.add_edge(last_node, block)
            last_node = block
            block = None

            if self.graph.number_of_nodes() > 10:
                break

    def get_block(self, i):
        assert len(self.code) > i + 2
        op = Operation(self.code[i:i + 3], i, self.code_object)
        i = op.get_target()
        block = Block(op)

        # TODO: put branch at the bottom of a block
        # TODO: remove old
        if not op.is_control_flow():
        # if not op.is_jump_directly():
            while not op.is_return() and i < len(self.code) - 2:
                op = Operation(self.code[i:i + 3], i, self.code_object)
                # TODO: remove old
                if op.is_control_flow():
                # if op.is_jump_directly():
                    break
                block.block.append(op)
                i = op.get_target()
        return block

    def get_blocks(self, block):
        blocks = []
        for offset in block.next_offset():
            if offset < len(self.code) - 2:
                blocks.append(self.get_block(offset))
        return blocks

    def extend(self, src, dest):
        self.graph.add_node(dest)
        self.graph.add_edge(src, dest)

    def gen_graph_recursive(self, last_block):
        if self.graph.number_of_nodes() > 1000:
            return

        assert isinstance(last_block, Block)
        if last_block.start_offset() in self.analysed_nodes:
            return

        self.analysed_nodes[last_block.start_offset()] = True

        if last_block.has_return():
            return
        # get blocks
        blocks = self.get_blocks(last_block)
        # add edge
        for block in blocks:
            self.graph.add_node(block)
            self.graph.add_edge(last_block, block)
            self.gen_graph_recursive(block)

    def gen_graph_loop(self, last_block):
        assert isinstance(last_block, Block)
        while True:
            if self.graph.number_of_nodes() > 20:
                break
            if last_block.has_return():
                break
            # get blocks
            blocks = self.get_blocks(last_block)
            # add edge
            assert 0 <= len(blocks) <= 2
            if len(blocks) == 0:
                break
            elif len(blocks) == 1:
                block = blocks[0]
                self.extend(last_block, block)
                last_block = block
            elif len(blocks) == 2:
                left = blocks[0]
                right = blocks[1]
                self.extend(last_block, left)
                self.extend(last_block, right)
                last_block = left
                self.gen_graph_loop(right)

    def remove_node(self, parents, rm, next_block):
        """
        remove and reconnect
        """
        self.graph.remove_node(rm)
        for parent_block in list(parents):
            self.graph.add_edge(parent_block, next_block)

    def replace_node(self, parents, replace, new_block, next_block):
        self.graph.remove_node(replace)
        self.graph.add_node(new_block)
        self.graph.add_edge(new_block, next_block)
        for parent_block in list(parents):
            self.graph.add_edge(parent_block, new_block)

    def optimize(self, block, divider=None):
        assert isinstance(block, Block)
        parents = self.graph.predecessors(block)
        neighbors = self.graph.neighbors(block)
        for next_block in list(neighbors):
            assert isinstance(next_block, Block)

            if block.should_be_optimized(self.varnames):
                # optimize JUMP_ABSOLUTE, JUMP_FORWARD
                if block.is_jump_directly():
                    self.remove_node(parents, block, next_block)
                # optimize DIVIDER
                elif block.is_divider_obfuscator(self.varnames):
                    new_block = Block(operation.nop_op(block.divider_equal(self.consts, divider), block.start_offset()))
                    self.replace_node(parents, block, new_block, next_block)
                    # self.remove_node(parents, block, next_block)
                    pass
                    # TODO: remove jmp
            self.optimize(next_block, block.update_divider(self.varnames, self.consts, divider))
            # else:
            #     self.optimize(next_block, block.update_divide(divide))
            #     if next_block.should_be_optimized():
            #         self.optimize(next_block)
            #     # combine
            #     else:
            #         self.graph.remove_node(block)
            #         block.combine(next_block)
            #         self.graph.add_node(block)
            #         for parent_block in list(parents):
            #             self.graph.add_edge(parent_block, block)
            #         for next_next_block in list(self.graph.neighbors(next_block)):
            #             self.graph.add_edge(block, next_next_block)
            #         self.graph.remove_node(next_block)
            #         self.optimize(block)
