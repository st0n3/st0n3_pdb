tracer
```
Hello 1
              0 LOAD_CONST          4 ((1, 0))
              3 UNPACK_SEQUENCE     2
              6 STORE_NAME          0 (a)
              9 STORE_NAME          1 (b)
             12 LOAD_NAME           0 (a)
             15 POP_JUMP_IF_TRUE    24
             24 LOAD_CONST          2 ('Hello')
             27 PRINT_ITEM     
Hello              28 LOAD_NAME           0 (a)
             31 PRINT_ITEM     
1              32 PRINT_NEWLINE  

             33 JUMP_FORWARD        0 (to 36)
             36 LOAD_CONST          3 (None)
             39 RETURN_VALUE   
```

debugger
```
> /home/st0n3/pentest_project/st0n3_pdb/sample.py(1)<module>()
(st0n3-pdb) s
[stack] []
              0 LOAD_CONST          4 ((1, 0))
> /home/st0n3/pentest_project/st0n3_pdb/sample.py(4)<module>()
(st0n3-pdb) s
[stack] [(1, 0)]
              3 UNPACK_SEQUENCE     2
> /home/st0n3/pentest_project/st0n3_pdb/sample.py(7)<module>()
(st0n3-pdb) 
[stack] [1, 0]
              6 STORE_NAME          0 (a)
> /home/st0n3/pentest_project/st0n3_pdb/sample.py(10)<module>()
(st0n3-pdb) 
[stack] [0]
              9 STORE_NAME          1 (b)
> /home/st0n3/pentest_project/st0n3_pdb/sample.py(13)<module>()
(st0n3-pdb) 
[stack] []
             12 LOAD_NAME           0 (a)
> /home/st0n3/pentest_project/st0n3_pdb/sample.py(16)<module>()
(st0n3-pdb) 
[stack] [1]
             15 POP_JUMP_IF_TRUE    24
> /home/st0n3/pentest_project/st0n3_pdb/sample.py(25)<module>()
(st0n3-pdb) 
[stack] []
             24 LOAD_CONST          2 ('Hello')
> /home/st0n3/pentest_project/st0n3_pdb/sample.py(28)<module>()
(st0n3-pdb) 
[stack] ['Hello']
             27 PRINT_ITEM     
Hello > /home/st0n3/pentest_project/st0n3_pdb/sample.py(29)<module>()
(st0n3-pdb) 
[stack] []
             28 LOAD_NAME           0 (a)
> /home/st0n3/pentest_project/st0n3_pdb/sample.py(32)<module>()
(st0n3-pdb) 
[stack] [1]
             31 PRINT_ITEM     
1 > /home/st0n3/pentest_project/st0n3_pdb/sample.py(33)<module>()
(st0n3-pdb) 
[stack] []
             32 PRINT_NEWLINE  

> /home/st0n3/pentest_project/st0n3_pdb/sample.py(34)<module>()
(st0n3-pdb) 
[stack] []
             33 JUMP_FORWARD        0 (to 36)
> /home/st0n3/pentest_project/st0n3_pdb/sample.py(37)<module>()
(st0n3-pdb) 
[stack] []
             36 LOAD_CONST          3 (None)
> /home/st0n3/pentest_project/st0n3_pdb/sample.py(40)<module>()
(st0n3-pdb) 
[stack] [None]
             39 RETURN_VALUE   
--Return--
> /home/st0n3/pentest_project/st0n3_pdb/sample.py(40)<module>()->None
(st0n3-pdb) 
[stack] []
             39 RETURN_VALUE   
> /usr/lib/python2.7/bdb.py(404)run()
-> self.quitting = 1
(st0n3-pdb) 
[stack] [None]
            155 LOAD_CONST          3 (1)
```